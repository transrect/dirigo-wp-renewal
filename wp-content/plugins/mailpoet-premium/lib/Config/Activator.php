<?php

namespace MailPoet\Premium\Config;

use MailPoet\Settings\SettingsController;

if (!defined('ABSPATH')) exit;

class Activator {

  static function activate() {
    $migrator = new Migrator();
    $migrator->up();

    $populator = new Populator();
    $populator->up();

    self::updateDbVersion();
  }

  static function deactivate() {
    $migrator = new Migrator();
    $migrator->down();
  }

  static function reset() {
    self::deactivate();
    self::activate();
  }

  static function updateDbVersion() {
    $settings = new SettingsController();

    try {
      $current_db_version = $settings->get('premium_db_version');
    } catch (\Exception $e) {
      $current_db_version = null;
    }

    $settings->set('premium_db_version', Env::$version);

    // if current db version and plugin version differ, log an update
    if (version_compare($current_db_version, Env::$version) !== 0) {
      $updates_log = (array)$settings->get('premium_updates_log', []);
      $updates_log[] = [
        'previous_version' => $current_db_version,
        'new_version' => Env::$version,
        'date' => date('Y-m-d H:i:s'),
      ];
      $settings->set('premium_updates_log', $updates_log);
    }
  }
}
