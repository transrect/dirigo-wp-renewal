<?php
add_action( 'wp_enqueue_scripts', 'magbook_enqueue_styles' );
function magbook_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-custom-style', get_stylesheet_directory_uri() . '/css/custom.css' );
}

?>
