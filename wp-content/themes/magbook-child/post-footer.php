<style>
	.post-footer .addtoany_share_save_container{
		display: none;
	}
	.post-footer .post-footer-book-tile,
	.post-footer .post-footer-movie-tile{
		margin: 10px 0;
	}

	.post-footer .post-footer-book-tile .card-cover{
		height: 450px;
		background-size: cover;
	}
	.post-footer .post-footer-book-tile .card-title{
		height: 100px;
		overflow: hidden;
	}

	.post-footer .post-footer-book-tile .card-text.book-desc{
		height: 100px;
	    overflow: hidden;
	    border-bottom: 10px solid white;
	}

</style>

<div class="post-footer">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<?php echo do_shortcode('[pods name="book" orderby="post_date DESC" template="Book Tile"]'); ?>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row">
				<?php echo do_shortcode('[pods name="movie" orderby="post_date DESC" template="Movie Tile"]'); ?>
			</div>
		</div>
	</div>
</div>